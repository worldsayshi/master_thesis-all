# Doc
- Meet feedback

# Code
- Sort out tricky properties at the begining of grouping component, workaround for below
- Field names need to be escaped in the data importer, 
- facet query does not exclude values not selected. Why!?
- Use faceted query on the propertyNames field for retrieving properties for grouping, instead of going through docs
	- example:
		- http://localhost:8983/solr/qalixaselect/?json.wrf=jQuery182025017478968948126_1361357760866&wt=xml&q=(dator)&facet=true&facet.mincount=1&facet.limit=60&facet.field=propertyNames&_=1361363902236
- attributes that are selected should not get removed when a new result is returned
	- problematic!
- don't split category names!
- fix sliders!
- jquery sliders are re-used for each render, adding some code complexity;
  perhaps it's possible to create a new slider with the same state?
- fix tokenization (categories!)
- All those get('x') may add an significant performence overhead? Somethings hogging anyway
- Adding/removing columns from the result table is slightly UX inconsistent

<!-- I think padlock is based on false premise:
instead of weeding out, highlight those that are now weeded out. Selecting them will add to results. Unless a new filter will be activated.. -->

### Into thesis:
- Currently only ordering of results are affected by the ordering 
  of the filters. There is a possible extension: Each concecutive 
  Filter should reduce the selectable values of filters that follow.
  However; doing so requires some rethinking model wise. 
  Doing so will allow filtering out any values that if selected 
  would cause an empty result list.
- Being able to request multiple facet fields for each request
  should make the above feature reasonably cheap in terms of 
  number of request per change. However, there are likely a pletora
  of strange edge cases waiting around the corner (no pun intended).
- Padlock problem
	- Disapearing filters is a subset of this problem that happens when all
	  values run out
    - Possible partial solutions: 
		  - unlock all other filters
		  - Don't remove, just mark values not usable

