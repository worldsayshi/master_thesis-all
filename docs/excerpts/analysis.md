### Analysis: Introducing a few usability heuristics

When interacting with an information retrieval or information exploration system, the user is occupied with the task of finding one or many sets of information. It is of interest to the user that this task is made as simple as possible. An interpretation of what simple means in this context follows. But first, we point out a few complementary heuristics. 

A graphical interface present a number of elements to the user. An element here constituting and a single unit of perceivable information, and sometimes one or more perceived or hidden affordances. Each piece of information adds a constant amount of complexity to the overall complexity of the interface. For each actual affordance added, there is also a non negligible amount of complexity added, although this depends more heavily on the users perception. We should distinguish between easily measurable structural complexity and the more elusive perceived complexity perceived and felt by an agent. 

Non perceived affordances should not in themselves cause added perceived complexity and mental load. Potentially perceived and well understood affordances may not cause very much affordances either. Mental load is instead expected to be caused by elements not communicating their affordances well enough, in such, the amount of hidden affordances are *perceived* to be high. It is expected that a perceived and well understood affordance will add a small constant mental load per item while a affordance that is not well understood could add a considerably higher load, in worst case causing the user to give up. 

Making sure that the maximum amount of hidden affordances are kept low and at the same time allowing such to be easily explored through interaction should help the user to quickly lower the perceived complexity of the application. An affordance should be easily explored if similar actions produce similar results and if the variation of perceived possible actions is low. 

For the sake of this project, designing a *simple* interface is interpreted to denote a minimization of above described complexity over the course of a task completion. A task can be more or less well defined. It could be described as 'finding a nice affordable car' or finding a car of a particular brand from a particular year of a particular color. This distinction, between a more and a less well defined task, is also the distinction between Information Retrieval, for well defined, and Exploratory Search, for less well defined tasks. [citation needed]

Two relevant heuristics can be defined for making sense of this idea of interface simplicity. *The amount of perceivable elements in a given scene* and *the amount of perceivable elements over the course of a task execution*. In order to minimize the latter, the former needs to be minimized, as well as the amount of scenes that needs to be faced by the user before the task is completed.

Simplicity is not the only requirement for a usability however. Understandability is also needed. Understandability can be seen as the ease of perceiving the affordance of an element given the element's and related element's presented information.

Another concern is that the user needs to solve his task quickly. Two things can be seen to consume time given the conceptual model so far, technical *latency* for switching and changing the scene, and time needed to perceive relevant elements in the scene.

#### Relation of precision and recall to affordance

When looking at affordance in the realm of exploratory search (ES) some related heuristics can be found. It should be desirable to encounter as little complexity during a task completion as possible, as described above. Another way of interpreting this in the realm of ES is that we should maximize precision and recall while minimizing the complexity of the interaction. 

Now, there can be many interpretations of this goal. One naive and often used solution is to present a single element of affordance with a wide space of action possibilities, or in concrete terms, a search phrase input form. Varying the amount of tools available in the environment could diminish the amount steps needed for finding a sought article, or it could potentially allow a faster growing recall curve. 

