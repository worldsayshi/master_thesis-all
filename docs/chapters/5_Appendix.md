# Appendix

## Source code
The source code for the prototype can be found at [@Thesis-Src-Repo]. It can be deployed and used by following the contained README. The instructions as well as known issues will be repeated below for completeness, but may not be up to date.

### Summary of repository contents
All these components are designed to work alone reasonably well. They should depend on each other only run time wise. Data importer depends on Solr. Grouping component depends on the data importer. Client depends on grouping component (although search should work without grouping, possibly you need to disable grouping in Solr config).

#### Flat table import handler (server/customComponents/dataImport)
For importing data from MySql database to Solr

#### Field grouping component (server/customComponents/fieldGroupingComponent)
For clustering of attributes when submitting a search query. Has two grouping algorithms that are switchable by a constant in the code (FieldGroupingComponent.groupingScheme).

#### Master thesis gui/web client (client)
Web front end that talks with solr. Using backbone, underscore, jquery and some jquery-ui.

#### Documentation
Source material for this thesis.

### Known issues
As far as known, all of the known buggy behaviors can be escaped by reloading the page.

- The gui does not update if the result of the query was null
- Clustering algorithm is throwing exceptions for some (small or empty?) data sets, causing failure to update
- Selecting some attribute values containing whitespaces or unconventional symbols may cause malformed query to solr, failure to update or wrong results
- Submitting an empty phrase (in search field) will return a result
- The maximum number of imports processed by the data importer is currently hard-set by an if statement in its main while loop. (Intentional for debug purposes)
- The multi valued-ness of properties currently needs to be set in two places in the solr configuration. In the configuration of the import handler and in the schema. It would be better if it could be set only in the schema.
- For some queries Solr responds with a null object. However, at other times 'no documents found' results in a result object with no 
- The current schema produce a extremely large database. Consider tweaking the schema and removing long descriptions from index. It is currently not possible to save the entire database to disk.

### Set up
### Assumptions
- apache 2 running on machine
- mysql 5.6>= running on machine with database adherent to config in server/solrconfig

### How to set up server
- fetch submodules
- run getSolr in server folder
- run runServer to start server
- fill up the database by making data import query to solr:
localhost:8983/solr/flattable-dataimport?command=full-import
- Check its progress by visiting:
localhost:8983/solr/flattable-dataimport
- If the data is begining to take up too much space, you can abort and commit with
localhost:8983/solr/flattable-dataimport?command=stop
 
#### How to set up client
- Configure apache to point at the client folder
- Go to localhost in the browser
- Use the gui!

#### Build data importer and grouping module
- You *should* not need to do this to run. There are pre built jar-binaries in server/solrconfig/lib.
- If you want to update the jars. Run the ant build scripts in each eclipse project, default target.

## Additional papers
Here are a few sources for relevant papers that were discovered to late for being utilized in the paper; they should be evaluated in future projects:

- Symposium on Human-Computer Interaction and Information Retrieval [@HCIR]
  
Papers that were not included but may be of interest are: 
- [@aanen2012schema] propose a system for mapping between taxonomies of products in e-commerce applications.
- [@yu1991determining] deals with interoperability of databases by correlating schemas using similarity measures.
- [@wauthier2012active] describe optimizations for spectral clustering, a method for allowing dimensionality reduction that may be relevant for cases where the input set consist of a large adjacency list.

<!--
### About spectral clustering
Spectral clustering is a clustering method for clustering data characterized by pairwise similarities and a specifically a method for allowing dimensionality reduction [@Wiki-spectral]. Dimmensionality reduction can be particularly interesting for the case of adjacencies as input parameters as such typically has a dimensionality as large as the number of items being clustered. [@wauthier2012active] expand on this and supply algorithms and methods for both better efficiency (in terms of performance) and effectiveness (in terms of quality) trying to alleviate the cost of computing such similarity measures using active learning.
-->

<!--

## Prospective headlines
~~~~~~~

# Preface
- Abstract
- Background/Introduction

## Prototype summary
- Pluggable facet filters
- Grouping algorithm
- (Solr configuration)

# Prestudy

## Methodology
- Literature, "interviews", prototyping

## Case study: Qalixa
- Unique challenges 
- Oppurtunities for improvement
	- Data/input quality
    - Performance
    - Interactivity
    - Code maintainability

## Theoretical summary
- Information Retrieval
- Usability

## Availible tools
- IR frameworks
- Client side tools

## Literature study
- Gathered ideas and inspirations from relevant papers on
	- Data mining, 
	- clustering, 
	- entity matching 
	- classification
	- Recommendation systems (realized by facebook integration?)
	- Exploratory search / Domain specific search?
- Focus on pointing out what ideas and papers can be applied in future work

## Stakeholder analysis
- Qalixa
- Academic challanges
- Readers of this article
- Learnings

## Suggested solutions
- User interface ideas
  - Mock ups
- Algorithmic ideas
- Theoretical motivations

# Prototype
- User interface implementation
	- use case analysis
	- overall architecture
		- improving maintainability
	- generalizing the concept for other use cases (Analysis?)
- Grouping algorithm
	- improvement prospects
	- implementation overview
- Solr configuration
- Data importer

# Conclusions
- Analysis
	- How well were oppurtunities met by the prototype?
	- Unmet oppurtunities and problems with prototype
- Future work
  - Inspirations/sources for future solutions
  - (new bottlenecks and prioritizations)

# Appendix
- links to source material
- instructions for use and deployment
- known issues
- other tools used: pandoc
- Mock-ups and ideas not included in thesis and/or link to disincluded material

~~~~~~~
-->
