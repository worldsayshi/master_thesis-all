<!-- # Preface -->

# Abstract #

Qalixa.com is a online e-commerce meta search engine that consolidate articles from a growing number of retailers from most lines of retail business. 
The articles in store entail a variety of meta data types such as properties, tags, category structures and free text. 

In this project, the author try to find ways to exploit the meta data and facilities available through a modern Enterprise search solution to create suggestions on how to build a User interface with sufficient competence to deal with this complex search space in an empowering manner, yet represent a simple enough experience for the end user to allow overview as well as a simple as possible learning curve.

A few concepts are prototyped and a variety of options of future work are analyzed. Characteristic issues with applying faceted search to the particular case are found and analyzed to some detail. Concepts that are tested to some extent are *selectable facet filters*, *grouping of properties with clustering*, *fuzzy logic in facet search* and *implicit ordering of documents given filter order*.

## Keywords ##

Faceted search, Enterprise Search, Dynamic properties, Human-Computer Interaction and Information Retrieval

# Introduction #

Qalixa is a small start up company that drives the development of a search engine hosted at qalixa.com. Qalixa aims in this engine to deliver a free-to-post advertisement service, both for the business to business and for the business to consumer segment. 

The purpose of this thesis is to find ways to bring further insight into the particular case that Qalixa represent in the scope of faceted search and user interface design for faceted search, and if possible present opportunities for improvement for Qalixa and for users of such technology in general.

This is realized by analyzing the most prominent issues with the current solution, proposing a set of concepts to alleviate these problems, implementing prototypes for these suggestions and finally evaluate these solutions to bring further insight into the challenges of the case. 

## Prototypes ##

The first proposed solution is that of *selectable facet filters*. It connotes presenting the user with a set of properties recurring in the set of results currently retrieved, and allow the user to select from these properties and in so turning the property into a *filter*. Filters are presented in a separate list from the properties. In such, the user is allowed to manage preferences of interest separately from a larger set of properties that are not of immediate interest. The filters can be *expanded* to present a set of applicable filter values for the user, allowing him/her to select a span.

Secondly, to deal with cases where the number of applicable properties are large, methods for arranging such properties in a logical way are needed. To supply structure properties were *arranged in groups using a clustering algorithm*.

In order to avoid false negatives in a high entropy and heterogeneous data base, it is desirable to have filters that does not work in a inclusive/exclusive but promoting/demoting manner. This method can be compared to the idea of fuzzy logic and is in this thesis referred to as *fuzzy facets*. In that way, articles that happen to lack definitions on particular properties that would otherwise be of the desired kind can be found in the list, albeit further down in the list. In the prototype and in this thesis a simple method for achieving this behavior is suggested.

Another feature is that of *implicit ordering of documents given filter order*. Given a moderate amount of interesting articles that the user wants to compare in detail, being able to compare properties closely should make it desirable to rearrange the order of articles based on values of properties. In this thesis a mean for arranging documents based on the order of filters in the filter list is proposed.

## Questions ##

- How can faceted search be exploited for cases where dynamic properties are prevailing?
- How can Solr or similar tools be used in a Qalixa?

## Tasks faced ##

- Production of several GUI concepts for enterprise search and for the e-commerce domain
- Evaluation of search frameworks
- Configuration of Solr for enterprise search
- Development of plug-in for optimization of MySql to Solr data migration
- Implementation of e-commerce search GUI prototype with focus on faceted search
- Analysis of problems with the prototype and conclusions taken
- Implementation of a clustering algorithm
- Formalization of a theoretical model for user centric search in the information retrieval domain

## Outline ##

This thesis describe the project by first describing the case being studied in chapter \ref{Background} after which some relevant methodology and tools for the domain are described in chapter \ref{Methods}. Then, in chapter \ref{Concepts}, some concepts where thought up taking inspiration from challenges and opportunities observed. Some of these concepts are turned into a working prototype, that is described in chapter \ref{Prototype}. The working prototype is then evaluated and problematized in chapter \ref{Analysis}. Lastly, the problems are discussed further and some ways of tackling the issues are suggested in chapter \ref{Discussion}.

