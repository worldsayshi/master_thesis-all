

# \label{Analysis}Analysis #

<!--
### Use case analysis
#### Case A 
- Finding a computer with (Vikt,1-3 kg),(Färg,svart|silver), change mind to black | pink
- Can easily and quickly change your mind
- Can fluidly check out variations on an attribute, to see what things drop off, to highlight those
- For exploratory search - what attributes should be shown that have not been chosen

#### Case B
-->

### Intended use case ###

The intended use of the prototype is to first enter a generic search phrase in the search form and hit 'Search'. Secondly, zero or more properties are selected from the left according to one's preferences. Thirdly, selected properties, now turned into filters in the filter tray above the result list, can now be used for narrowing down the results. Selecting multiple values from the same property will cause a non-exclusive multi-value selection on that property. Or in other words an 'OR' selection will be made with all selected values in one particular property. If multiple filters are used, an exclusive selection will happen, combining the filters with 'AND' operation.

The intention is that both retrieval of specific known articles, using known properties of such articles, and exploration of the data set within a certain domain, should be made easier. Allowing direct feedback when alternating both properties and property values should allow exploration while supplying relevant properties should allow the user to quickly zoom in on an intended article. Additionally, adding and subtracting filter selections should allow the user to identify corner cases, highlighting what articles the user is opting out.

### Observed issues ###

Some issues can be observed when looking at the prototype. Issues that are considerable in scope but that if solved, should significantly elevate the potential of the approach suggested by the prototype. Some of these challenges are arguably hard to avoid for any attempt at dealing with e-commerce from the perspective chosen by Qalixa and similar ventures, and should emancipate considerable value if solved comprehensively. Many of these issues are expected to be especially relevant when having a great number of dynamic properties coming from varying sources.

The most apparent problem in the prototype is the high amount of property fields that are available for being selected from by the user. Many of the fields are nonsensical when taken out of context of their parent articles or seem to be irrelevant for most uses. Many properties and values are duplicates or are of very similar meaning. Sometimes property names can map to values of varying type or dimension. Some values may be of number type but this is not recognized and as such, the property is not given a slider when used as a filter. Some properties have superfluous characters in their name. Some values are placeholders for non-values, some should be split into multiple values. 

Many properties have only a single selectable value. This is arguably a part of a larger problem. At the point of retrieving properties, the amount of applicable values for each property given the current search space is not known (only the number of occurrences of the property fields throughout the result set is known). Retrieving applicable values for a large result set for all properties is supposedly very computationally expensive, however possible, using the Solr REST API. With the current prototype, only the values of the chosen filters are retrieved.

More subtle are the issues that arise once you start the interaction. One possible issue is that of filtering out false negatives. If a certain attribute is not defined for an item it can not really be known if it really should be filtered out or not. The default behavior of Solr is to filter it out, which may be undesirable. Similarly, a lacking in synonym understanding among values should cause problems of false negatives as well.

#### Dynamic filter options and *Padlocks* ####

One issue appear as a result of updating selectable filter content dynamically. As the result set is diminished, the set of field-value pairs normally diminish as well. If this is not reflected in the possible preferences of the filters, there is a risk of allowing the user to select a set of preferences that results in an empty result list. In some cases this risk may even be higher than selecting preferences resulting in a non-empty result set. 

Always allowing dynamic updates of filters is problematic since the filter mix the trait of excluding existing or including additional articles. When first putting a filter into use, the filter acts to exclude items from the result set. When selecting additional values, the result set grows. Similar growth and shrink pattern is reflected in the set of applicable values and properties. Additionally, whilst using fuzzy facets or working with a large set, the issue is a bit more subtle. Individual articles, properties and values may be excluded by way of becoming less prominent as the set grows. As such, filters or values will become treated as being to insignificant for inclusion, causing it to disappear mysteriously.

The Padlock feature exist as to partially adress this problem. To help the user to avoid these cases the padlock button allows the user to manually switch whether applicable filter values are dynamically updated. This is far from a perfect solution as it requires the user to figure out when to apply the dynamic him/herself.

<!--
As the result set diminishes, the set of values and properties that appear in the result set will also diminish. This represent a problem if the filters update according to the set. If so, values that would allow expanding the result set would be made impossible as soon as -->

#### Summarized, the issues in list form: ####

- Missing context for understand property meaning
- Irrelevant properties
- Synonyms not recognized
- Unit or qualitative/quantitave homonyms not recognized
- Basic sanitization and interpretation of names and values are lacking upstream
	   - Special characters in property names
	   - Multi values interpreted as single values
	   - Non-value placeholders interpreted as values
- Insufficient structuring of large amount of properties
- Over-eager filtering, false negatives
	   - Synonyms
	   - No defined value for selected property
- Invalid or missing options among filters after query

### Technical issues ###

Some issues are purely technical. Some property names cause problems due to Solr not being able to cope with special characters. This could be blamed on insufficient sanitization but there may be cases where special characters are desired in the displayed name. Additionally, many non-english languages will require support for Unicode. For these purposes Solr have some support, using the ASCIIFoldingFilterFactory [@ASCIIFolding] or UnicodeCollation [@UnicodeCollation]. These are not applied in the prototype however.

Another issue is the need to associate and infer additional information to the property name, such as property type. The variation of property types may for example be used for varying visualization and interaction methods, so that properties with number values may use sliders. Associating additional data to field names may require creating additional entries to the database. Such entries could also associate a display name with special characters with a look up name, without such characters, if needed.

### Comparison to current property view of Qalixa.com ###

![Diagram comparison of visual complexity \label{beforeAfter}](../img/scaled/small/before-after.png)

Previously, the current element for selecting among properties and property values were described briefly (See fig \ref{PropView}). Analyzing the current solution while taking inspiration from the suggested usability heuristics we can make the following observation. For the task of allowing the user to select preferences given applicable properties the current solution present a number of visual elements roughly equivalent to the number of properties times the number of values on average. The proposed solution present a number of visual elements roughly equivalent to the number of properties plus the number of selected properties plus the number of values of the property currently being in focus. This amount is strictly smaller in general and is expected to be significantly smaller for most cases (See fig \ref{beforeAfter}).

Furthermore, the ratio of visual elements shown clearly to the user, high up on the page, that are of direct interest versus such elements that are of secondary interest, should also have improved significantly. *Filters* corresponding to the users preferences are maintained in plain site and does not remain among the properties in the property list.

# \label{Discussion}Discussion #

The prototype tested a few of the ideas presented to some degree. Creation of custom filters where tried to some extent, but discarded due to adding little purpose to the prototype in its current form. The most prominent concept in the prototype is that of the filter tray. Dynamic filter lists were used as well although it was hard to make conclusions about its usefulness.

Apart from the suggestions, the project served to investigate the potential of Solr and similar technologies for Qalixa and it brought a variety of new lessons on web technology, such Javascript frameworks, databases and search engines, to its author.

<!-- Previously: 'Other Observations' -->

Given that relevant properties can be found in the property list and given that they house sensible values, some observations can be made about the benefits of the prototype and its approach. 

Once properties have been selected, distance in between useful components is small. This is beneficial for exploring small variations on the same properties. Having sliders for number properties allows for fast exploration. Having low latency for queries when switching properties on and off allows for a fast feedback loop on what selections fall off as the scope is narrowed.

Also it can be seen that the set of results are quickly narrowed down as filter scope is narrowed down. The problem of drop off due to false negatives will still be a problem even with fuzzy faceted search though. There will be likely be a bias towards articles that has well defined properties. Although with fuzzy faceted search, recall should become sufficient. Precision due to variations in how article properties are defined are to be expected. On the other hand it is arguably so that an article is more likely to be incorrectly excluded/demoted than included/promoted when filtering on a property. Overlooking the act of setting a property seem more likely than setting the property wrongly.

It is observed that being able to quickly alter between adjusting various property value scopes is useful. This is partially accomplished by the prototype. Although the later prototype only allows having one active, as in adjustable, filter at a time. Earlier snapshots allowed filters to be expanded and collapsed at will. This makes the scene slightly less complex but also hinders switching between filters quickly.

Having interesting filters clearly separated from the overall property list also seem to make user preferences manageable. Although here is an ambiguity regarding the persistence of filters. What should happen when the filters are no longer relevant for the search phrase? The current behavior of the prototype is to remove such filters. This can sometimes be problematic. A better choice is perhaps to render those filters inactive and mark them as such. Allowing the user to remove them at will.

There was an early attempt at allowing the user to select filters by drag and drop, as suggested by the suggested concepts above. This was deemed to supply little value and some distraction for the user, and was discarded in favor of simply clicking elements for turning them into filters. A more complex web of possible interaction potentials between various components, where elements can move between various parts to a greater extent and in a meaningful way, could make drag and drop useful again.

The order of the result list is in the prototype determined by the order of used filters in the filter list. While this may be a useful feature in some edge cases, it is deemed that this is not a practical default behavior that communicates badly to a user. Having explicit controls to enable this behavior may be useful, although it is likely that the de facto default behavior of most result lists out in the wild, to sort if the column header is clicked on, is more intuitive due to its strong history. However, the method suggested here have the potential advantage of sorting on multiple criteria. This could be useful but likely only in periphery use cases.


<!--
## Conclusions ##

Summarizing and prioritizing problems? Superfluous?

- Faceted search require a better structure in property meta data 
	  - Creating associations between the category system and individual properties
	  - Discerning homonyms and synonyms
=> So focus on meta data coherency should be highly prioritized
- The structure of the data needs to be reflected in the GUI
	  - For adding context to property names
	  - For sorting out irrelevant properties
	  - "For appreciating the space of options available"
=> Analyzing how to best map content to presentation is important
-->

## Suggestions, for academia, industry and for Qalixa ##

To establish structure and coherency of the meta data, a few things are suggested for further investigation and for solving related problems in broad terms.

### Pruning and sorting properties using statistics and traits ###

Two methods for grouping properties are recognized. Properties can be grouped by categories and grouped by traits. *Category* here denoting and assuming a preexisting grouping of articles in a category tree. Such structure can be carried over to the properties so that each category is tied to the categories that occur in the articles that are sorted into it. If a category has been selected by the user, properties can be shown based on whether they belong to that category as properties that exist outside the category are likely irrelevant. Further, properties that are part of a subcategory or a smaller subset of articles in the current category may not be very relevant either as a filter that would use such category would only be applicable on a small part of the articles. A generalization of this idea would be that of finding applicability of facet properties in a given selection, regardless of having a stated category. Bear in mind here that it might not only be properties that can be found in the current selection that might be of interest. Similarly to how non-selected values may be of interest, non-used properties may be of interest too, once the user looks to expand his/her preferences rather than diminishing them.

We could introduce a notion called *facet traits*. A facet property may have more or less values, and values may be of certain types. A property convey a number of applicable values for a given selection, the values that exist in the selection, but other values that may be of interest may exist throughout the database. Facet traits may be made to connote the characteristics of the its values, throughout the database on one hand, and given a selected context, on the other. Such traits can be used to further determine how a property is to be presented. The *facet trait* idea can also be extended to connote the above described property characteristics. To how large extent of the current selection does the property exist? To how large  extent of the entire database and in what categories does the property exist? So such traits are in part consistent regardless of context and in part context dependent.

Having such trait information available could help in building the presentation and allow qualified decisions on what to present and how to build user interface elements. Examples of using such traits could be:

- Turning single valued facets into keywords
- Hiding facets that exist for a small subset
- Show facets that does not qualify for the current selection but for a relevant superset, for example a category

<!-- Rewritten above
--### Prune and sort properties by category ###

This approach is used to some extent in existing Qalixa production system. Although the current solution is not optimal. Such functionality could potentially be further improved. By using statistical methods to tie properties onto specific categories the number of properties that have to be shown at a time can be controlled in a qualified manner. Applying early analysis could select which properties are meaningful for the domain given carefully selected heuristics. Property selections that are relevant to either a superset or subset of the current selection may not be relevant for the current selection. Properties that exist for all given elements but with little variety in their values or properties that exist for only a small subset of elements are examples of potentially non-relevant ones.

Heuristics serving as proxies for relevancy and popularity could be used to prune the set of properties available for filtering. Taking such approach could potentially take away properties useful to some users. It could be desirable however, to have pruned or hidden facets be selectable by some form of sub menu or by searching explicitly among applicable facets. The most apparent qualification for pruning would be occurence in the result set. A property that exist to a small degree in the retrieved documents might be deemed less useful. However, in such case it could perhaps be useful to have such property name as a value of the keyword property instead, allowing the user to filter out articles that specifically define that property.

The motivation in terms of earlier proposed heuristics framework can be that the amount of elements that are presented to the user is diminished and that the proportion of elements that are actually relevant to the user is likely to be higher. Th factuality of the last statement is naturally not given but has to be tested in future work. The choice of removing a property in a given context needs to be motivated by well chosen heuristics.

--### Structure properties by traits ###

As a concept closely related to pruning and to some degree extending on it is the idea of managing applicable facets by a set of traits that can be inferred about them. Examples of such traits can be, single valued for current search filtering, ubiquitous in current filter setup, number valued or multi valued - meaning that a single document may select multiple values for the same property. 

In order to infer traits that depend on the type, or quantity of its values some additional pre-processing may be required. While it is possible to compute such things at the time of query it should require many costly requests to the search server. However, an additional problem is that pre-computed traits will lack sensitivity to the current filter selection. A property that has multiple possible values throughout the database may still be single or few-valued for the current search result.

Some traits could require understanding of the property in the context of the entire database. For example, properties such as 'price' or 'category' is more or less ubiquitous throughout the database. The trait of being database-ubiquitous may be applicable to some dynamic properties as well. Other traits of similar kind, defined by the larger context, may be found as well. Some properties, like 'color', may exist for a large subset of multiple categories.
-->

### Controlled folksonomies ###

Having folksonomies, or wiki-like tag systems can be a useful way to establish structure in a large data set where items are interacted with by users on a regular basis. But to find the best way to harness it can be a challenge. [@dattolo2011integrated] explain different algorithmic methods for extracting good tag sets from folksonomies. Taking synonyms and similar problems into account.

### Client side facet search ###

in order to elevate the potential of facet search, and to allow fluid filtering for the user even if the server load is considerable, using client side facet search could be desirable. There are implementations for this available that show great promise. While client side search could not realistically cover the breadth of a server based solution, it can serve as a complement that works on narrowing down the set of results given by the server. The server could be allowed extended latency and fill up with new results only when the client side result set is draining out. [@Backbone-faceted] [@Backbone-faceted-demo]

### Grouping facets by outsourced semantics ###

One promising approach to enhance structure is to utilize outside sources of semantic structure, so called semantic web technologies. For example, DbPedia [@DbPedia] is an initiative for extracting semantic data from Wikipedia. A derived application, DbPedia Spotlight, can infer context to words in a text. Inspiration can also be taken from the way a prototype, Sztakipedia [@Sztakipedia], helps the user by suggesting  meta data to add to an article while being worked on. Such concept could be used in conjunction with the percolation concept suggested earlier. 

### Annotating rather than excluding invalid filters and values ###

The prototype combine different approaches in order to deal with invalid filters and values. Filters are removed when no longer needed. Values can either update dynamically or remain untouched. Such approaches are arguably confusing. A better solution, until a comprehensive solution is found, would be to mark options depending on the effect using them would have. Unused but selected filters that represent fields no longer found in the result set could be marked red for invalidity. That way the user can easily backtrack if that property was of interest. Values of an active filter that represent articles that are not in the result set could be marked blue to communicate a potential addition.

### Multi touch interaction for facet navigation ###

A suggestion made by [@Morten] is to use multi touch devices for allowing additional levels of freedom when exploring facets. In the current prototype, only a single preference can be adjusted at a time. The concepts described above suggest allowing moving in multiple dimensions for allowing the user to explore multiple dimensions at once. By allowing adjusting multiple preferences at once, preferably with sliders, utilizing multi touch capability, another variation is possible. The method would however be limited to such capable devices.

