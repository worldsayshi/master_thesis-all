# \label{Background}Background #

The project consisted of literature studies, concept creation, configuration and implementation of a prototype and finally analysis of the implementation and prospective future work, where a concrete list of issues of the domain could be produced. In the literature studies, the author tried to find potential ways to tackle perceived technical problems of the case in question, search at Qalixa.com. Theoretical studies was made in part as a prelude to the implementation task and in partly after the task, to make use of the lessons learned.

<!-- Stakeholder analysis -->
From the outlook it was explicitly of interest to test new search framework technologies. The scope of this project start out from the premise of applying one such technology on the existing database of Qalixa. Another interest was to try out interesting features of this framework to evaluate the opportunity it can bring. 

Furthermore, as an academic project, it was of some interest to try to find a research potential. And for the authors sake, to try out interesting technology for the sake of learning.

<!-- About user studies --> 
While user studies could have brought some additional insight and lessons from the prototype, it was outside the scope for the project due to time and resource constraints. It was also deemed that using such method would be premature due to problems with the user experience stemming from the domain problems identified.

Additionally, little data is available from current user behavior. Facilities for analyzing such is not yet in place as the Qalixa venture is still a young one.

In future works, user studies on a prototype using coherent input data can be used for adjusting characteristics of the user interface. And to give an idea of the understandability of the proposed features. Given well designed user tests; it could be concluded whether exotic features actually shorten the time for task completions and how likely such features are to be discovered and understood spontaneously.

#### Case study: Qalixa ####

Looking at the Qalixa use case a few characteristic opportunities were found. Technical opportunities that are perceived to need a lot of thought in order to leverage the value of the web service. Applying faceted search onto Qalixa present a certain challenge; the number of applicable facet properties can vary greatly between queries, as would show by analyzing the prototype. The total number of properties spanning all articles is unbound as each article can define and carry an arbitrary amount. The relevance and usefulness of any particular property is unknown from the outset.

#### Challenge: Empower the user in a complex search space; meta data interactivity ####

![Current property view at Qalixa \label{PropView}](../img/scaled/large/qalixa_prop_search.png)

By its nature, Information Retrieval involves navigating a large search space. Most of the time, the user is left without a map other than that of intrinsic experience derived from previous interactions with retrieval systems as well as his/her mental model of various searchable concepts. There is little aid for perceivable affordance from the outset. In many enterprise search solutions, or verticals, faceted search is a way to provide a map over the search space. The specific qualities of such map in the Qalixa case will need to be exploited.

For text input alone the visual complexity is relatively small. But for faceted search among an arbitrary amount of properties the GUI can become crowded. There needs to be ways of lifting out the most relevant properties given the information available.

One particular interactive element from the current Qalixa web site can be examined for this particular challenge. When browsing by category, a property view can be called up. The property view allow the user to specify preferences and to filter out unwanted items from the set. Visually, the user is presented with a table with a row for each property and cells for each value. Such presentation is common for such use case. However, for some categories the table become impractical as is grows to hundreds of rows (See fig \ref{PropView}).

#### Challenge: Performance and latency ####

In order to convey affordance, a tight feedback loop is required. Low latency is thus a high priority in any Information Retrieval application. Qalixa has few special characteristics in this regard. Current solutions are deemed inadequate in this regard. While a throughout performance profiling investigation of the current solution likely could help alleviating the situation it is chosen to be outside the scope of this thesis in favor of investigating alternatives. It is deemed that a technology switch is required for solving those problems.

#### Challenge: Information structure and coherence; meta data coherence ####

The problem of structuring data is in large ubiquitous to Information Retrieval but takes a certain shape when addressing faceted search. Certain problems, like finding synonyms and homonyms, are more pronounced. Merging properties can be seen as a superset of the problem of merging tags. Other related problems are automated categorization. Automated categorization can be applied to documents given statistical models where content is analyzed for finding a likelihood of an object belonging to a certain category. For this case, there is the additional problem of categorizing the meta-data itself, like properties.

While this project will only briefly test out a possible solution to this problem, some effort has been placed into looking for potential future work and recommendations.

# \label{Methods}Methods and Tools #

Some inspirations that went into the concepts produced are described below. Concepts such as affordance, precision and recall are important. Additionally, some heuristics were used as inspiration as well as for evaluating the prototype. The heuristics draw their motivations mainly from the idea that a good user interface should tax the attention capacities of the user as little as possible for any given task and that any task completion should be made quickly and with few and small steps.

<!--
- Books[@IRBook][@IntrIRBook]
- Course material[@IRCourse]
-->

## Usability - Affordance ##

The concept of affordances was introduced by James J. Gibson and is commonly interpreted as *action possibilities in an environment given an actors capabilities*. It can be divided into a few subcategories, perceived, as affordances that is known by the actor, hidden, as those that are not perceived and false affordances, as those that are perceived but not actual. [@Wiki-Affordance]

## Information retrieval - Precision and Recall ##

Precision and Recall are central concepts for evaluating information retrieval systems. Precision denotes a measure of the fraction of returned results that are relevant to the query. Accuracy denote the fraction of all relevant items that are contained in the result set. These terms imply a common method for evaluating IR systems, by measuring precision and recall of IR systems they can be evaluated against each other. [@Wiki-Precision] However, for testing purposes, this method requires a pre-made gold standard. A set of items and queries with relevancy mapping.

## Faceted search: categories, tags, properties and values ##

Faceted search is the name of an Information Retrieval related HCI methodology where the user is allowed to navigate the search space by selecting properties and values in order to narrow down the search result. Technically any field with a value belonging to a document in the search space can be used as a *facet*. Examples of facet fields can be "keyword", "tags", "length", "description", "color" etc. 

## Introducing a few usability heuristics ##

When interacting with an information retrieval or information exploration system, the user is occupied with the task of finding one or many sets of information. It is of interest to the user that this task is made as simple as possible. An interpretation of what simple means in this context follows. But first, we point out a few complementary heuristics.

A graphical interface present a number of elements to the user. An element here constituting a single unit of perceivable information and sometimes one or more perceived or hidden affordances. Each piece of information adds a constant amount of complexity to the overall complexity of the interface. For each actual affordance added, there is also a non negligible amount of complexity added, although this depends more heavily on the users perception. We should distinguish between easily measurable structural complexity and the more elusive perceived complexity perceived and felt by an agent.

Non perceived affordances should not in themselves cause added perceived complexity and mental load. Potentially, perceived and well understood affordances should not cause very much affordances either. Mental load is instead expected to be caused by elements not communicating their affordances well enough, or in other words, the actual affordance can not be unambiguously determined. It is expected that a perceived and well understood affordance will add a small constant mental load per item while a affordance that is not well understood could add a considerably higher load, in worst case causing the user to give up.

Making sure that the maximum amount of hidden affordances are kept low and at the same time allowing such to be easily explored through interaction should help the user to quickly lower the perceived complexity of the application. An affordance should be easily explored if similar actions produce similar results and if the variation of perceived possible actions is low. 

For the sake of this project, designing a *simple* interface is interpreted to denote a minimization of above described complexity over the course of a task completion. A task can be more or less well defined. It could be described as 'finding a nice affordable car' or finding a car of a particular brand from a particular year of a particular color. This distinction, between a more and a less well defined task, is also the distinction between Information Retrieval, for well defined, and Exploratory Search, for less well defined tasks.

Two relevant heuristics can be defined for making sense of this idea of interface simplicity. *The amount of perceivable elements in a given scene* and *the amount of perceivable elements over the course of a task execution*. In order to minimize the latter, the former needs to be minimized, as well as the amount of scenes that needs to be faced by the user before the task is completed.

Simplicity is not the only requirement for a usability however. Understandability is also needed. Understandability can be seen as the ease of perceiving the affordance of an element given the element's and related element's presented information.

Another concern is that the user needs to solve his task quickly. Two things can be seen to consume time given the conceptual model so far, technical *latency* for switching and changing the scene, and time needed to perceive relevant elements in the scene.

### Relation of precision and recall to affordance ###

<!--
- Minimize irrelevance : complexity is bad. Irrelevance is complexity without value
- Maximize value: Maximize average relevance per component
- Maximize relevance per surface
- Maximize relevance per perceived item
- Maximize relevance per surface and spatial attention span 
- Relevance : (precision * recall) ?
- Likelihood of relevance given a phrase !?
- Likelihood of relevance given no phrase ?
- Likelihood of relevance given more or less specific phrases?

- Possibly useful concepts
	- min permutation distance (scene1,scene2) -- inspired by levenstein distance
	- dScene = 'amount of visual complexity added' * C1 - 'amount of visual complexity removed' * C2
	- 'heuristic for mental load A' = SUM(i <- 0;nrOfSceneChanges)(dScene_i)
-->
When looking at affordance in the realm of exploratory search (ES) some related heuristics can be found. It should be desirable to encounter as little complexity during a task completion as possible, as described above. Another way of interpreting this in the realm of ES is that we should maximize precision and recall while minimizing the complexity of the interaction. 

Now, there can be many interpretations of this goal. One naive and often used solution is to present a single element of affordance with a wide space of action possibilities, or in concrete terms, a search phrase input form. Varying the amount of tools available in the environment could diminish the amount steps needed for finding a sought article, or it could potentially allow a faster growing recall curve. 

## Glossary ##

-------------   ---------------------------------------------------
Lexicography    study or structure of word relatedness on the 
	            basis of semantics and word features
Taxonomy        systematic classification of biological organisms,
                also classification of information entities in 
				general
Semantic        the meaning of a word
Ontology        a study or structure of meaning of words
Homonym         two words of equivalent spelling but 
	            with different meaning
Antonym         word with opposite meaning to another word
Polysemy        word that can have multiple related interpretations 
-------------   ---------------------------------------------------

## Literature study ##

Various articles where studied to acquire inspiration and finding potential inspiration for the Qalixa case. A few where selected for having future potential for the case.

[@liu2010crew] demonstrate methods and analyze a prototype implementation, CREW, for using Wikipedia as a source for creating semantic data and deriving homonyms and synonyms for tags. This could be used for processing untidy data from third party sources and to aid single article publishers to clearly define what they mean. Also, building taxonomies and categorizing new articles could potentially benefit from such technology. A cited article [@witten2008effective], goes further into details on how to obtain *semantic relatedness* from Wikipedia's network of links. 

[@hu2012personalized] details various methods for semi-automatic clustering; giving the user the ability to influence how clusters are formed through a proposed user interface. This could be useful for giving retailers and users the ability to control how their articles are categorized. [@chen2004clustermap] suggest another method that focus in particular on leaving the labeling task up to a supervising user. 

[@dumais2003stuff] design and analyze a GUI application, *Stuff I've Seen*, for managing search history in combination with a faceted search. The application allows users to conveniently search through articles previously viewed. The application is evaluated with extensive user tests.

[@turetken2004development] try to attack the problem of information overload by utilizing clustering techniques and corresponding GUI design. A clustering based visualization GUI for navigating a large set of items is presented. The problem of discerning concepts of varying significance is addressed by a *fisheye* concept where more significant words take up more space in the GUI.

[@kanungo2002efficient] present an optimized variant of Lloyd's algorithm using kd-trees. Lloyd's algorithm is a popular algorithm for clustering [@Wiki-lloyd].


## Tools ##

While there are numerous examples of open source frameworks for information retrieval, and while a exhaustive list is hard to find and probably impractical to maintain, a few frameworks came out as interesting.[@wikiListEntSearch] 

A few distinguishing traits can be seen among the alternatives out there. Firstly, there are either open source or closed source alternatives. Closed source alternatives where dismissed at an early stage. The motivation could be that it would narrow down future freedom for the project and Qalixa if put in use, another motivation is that of project scoping and the need to being able to quickly evaluate the solution.

Secondly, many of the search systems out there seems to be either ports or projects building upon Apache Lucene. Examples of the former are Ferret[@Ferret] and Lucy [@ApacheLucy]. Examples of the latter are Solr and ElasticSearch. 

Thirdly, frameworks are either built upon an existing database technology, or built as a database system of its own. Lucene and its derivates is an example of the latter, while Sphinx and Riak search could in part be seen as the former [@SearchComp2]. Also, Hibernate search could be grouped with the former with a stretch.

### Lucene and Solr ###

All in all, the Lucene family was perceived as the group of engines with the most prevailing community support and at a glance, the largest feature set of the engines looked at. Lucene itself stands out in that it is built for being integrated into a host application[@LuceneTutorial] rather than being stand-alone out-of-the-box deployable like Solr[@SolrTutorial]. Lucene requires you to at least build a custom data import layer in a jvm language while Solr allows you to design an import layer meeting many use cases in XML. Similarly, handling of the input query and the process of turning it into a query understandable by the framework is slightly more involved or more manual in the Lucene case. For Lucene, components are combined in code while Solr lets you combine components in XML while being guided by a systematic markup style. Lucene arguably gives you some additional freedom in defining the interface to the search framework, while its derivates adds additional features [@SolrFeatures][@SolrVsElastic]. 

Features of Solr include faceted search, geospatial search, XML/JSON/CSV REST-like api, XML configurability. Lucene also cover faceting to some extent [@Lucene-faceted]. Among web services that are currently using Solr are Instagram, The Guardian, SourceForge, eHarmony, Netflix, eBay (Germany), digg and reddit [@PublicServers]. Solr supplies means for running clustering on search results and documents using the Carrot2 clustering engine [@SolrClustering]. It is meant to be highly customizable, allowing plugging in implementations of for example clustering algorithms. Solr has shown to give significant speedup for large scale services, such as Twitter[@TwitterSearch].

### ElasticSearch ###

ElasticSearch is another engine in the Lucene family and the closest competitor to Solr. While it is younger and such would have had less time to mature, it seems to have gained a lot of traction. It sports a few features that are by some deemed as game changing. It is built from the ground up with distributed search in mind [@ElSearch] and has been shown to perform significantly better in some cases where a large index was continuously updated while also performing queries [@Elas-vs-Solr]. Solr is however deemed faster for indices that seldom change. ElasticSearch is made for easy deployment and it doesn't require defining a schema for basic use cases [@Elasticsearch]. An additional feature is the Percolator, or reverse search, that allows storing queries and then check which queries are matched by submitting a document [@Percolator]. Among web services that are currently using ElasticSearch are Mozilla Foundation, SoundCloud, StumbleUpon, Klout, IGN, Sony Computer Entertainment [@ElasticUsers] and StackOverflow [@StackExchangeElastic]. Developer usability wise ElasticSearch is customizeable by the same JSON REST interface that everything else is passed through. This could be a potential advantage to the rigid file XML file structure of Solr [@Elasticsearch].

### Sphinx ###

Another search framework is Sphinx. Some distinguishing features when compared to the Lucene family are tight integration to relational databases, for example MySQL, and a focus on speed, being seemingly faster than Solr for some tasks such as indexing and database queries[@SphinxOrSolr2]. In fact, one benchmark, albeit potentially outdated, shows Sphinx consistently outperforming Solr in terms of both memory and CPU [@RailsSearch]. Sphinx's facilities for faceted search are a bit different. Allegedly allowing a greater set of use cases but with seemingly a bit more manual work before running[@Sphinx-facets]. Another distinction is the source code licence. Solr is Apache licenced while Sphinx is licenced using GPL2 while the maintainers offer commercial licensing as well. Sphinx is used for such services as Craigslist and Groupon and is known to work with more than 25 billion documents and more than 300 million queries per day [@AboutSphinx]. Sphinx is written in C++ which may bring additional entropy to a Java centric project.

### Solr extensions and supporting applications ###

Some tools that can be used to extend Solr is worth a mention. SolrJ is a java library that serves as a java search interface to Solr that is otherwise talked to using a REST API. Using SolrJ some of the flexibility of Lucene can be recovered [@SolrJ1]. Tika is a parser library designed to work with solr, although it is mainly aimed at extracting data from documents [@SolrJ2]. Zookeeper is a framework for coordinating distributed processes. It is planned to become an integrated part of Solr for version Solr 4[@SolrRelease4]. Apache Nutch is a search engine solution that adds a crawler and integrates Tika with Solr [@AboutNutch].

### Search framework comparison ###

For this project, Solr was selected. This is not an obvious choice however. Three frameworks are seen as runner ups - Solr, ElasticSearch and Sphinx. It is appreciated that the choice of one very much depends on the problem. On the basis of scalability, ElasticSearch seems like the best choice at the moment, due to it being built with distributed storage in mind. However, Solr is currently spearheading an initiative to integrate cloud capabilities more tightly [@SolrCloud]. Solr sports its clustering engine. Although clustering could be seen as a feature of uncertain current footing in the domain, it may hold future potential. Solr is arguably the platform of choice for experimenting with such techniques, given its Carrot module. Performance tests of Sphinx show promise and the fact that it is used by some large scale e-commerce vendors could be enough to motivate testing it out.

### Search frameworks as NoSql stores ###

Solr and its cousins are structurally very similar to NoSql data stores [@Solr-nosql]. The distinction is arguably one of marketing strategy [@Solr-nosql-SO] although there may be some distinctions in the feature set as well [@Solr-nosql-SO2]. Likewise, some data stores implement many features of search frameworks [@SearchComp2].

### Backbone ###

Backbone is a javascript module and framework for lightweight MVC modeling. It is regarded as a library rather than a framework given that it imposes little restriction on the software architecture[@SevenFrameworks]. It supplies classes (or an approximation for classes) for such things as Collections, Models and Views and facilities for controlling event propagation between them.

### Pure Functional and Functional Reactive programming for client side web applications ###

Some time for shallow testing was given to the Elm language [@ElmLang], a new programming language that borrows ideas from the pure functional world of Haskell and that focus especially on the idea of Functional Reactive programming (FRP) [@FRP]. It compiles to Javascript and is meant to be used for developing graphical web clients. While a more throughout investigation of the pros and cons of using such a language for a complex web application is left outside the scope of this project, a few remarks can be made: The succinctness of implementations of some recurring web application features when using Elm can be demonstrated in the examples on its official web site [@Elm-examples]. The Flickr Api example should be the one most relevant to Information retrieval clients[@Elm-examples-flickr]. However, small implementation tests showed that debug printouts can be very sparse, potentially making continuous implementation challenging. 

An alternative to Elm that was also tried to some extent was Fay [@FayLang]. Fay takes the approach of implenting a subset of Haskell for compilation to Javascript. A second important design choice is to make the code generated as readable as possible, potentially making debugging easier. However, Fay does not give the FRP paradigm any precedence, but there is an example on how FRP can be realized using javascript FRP libraries in Fay [@Bacon-Fay].

# \label{Concepts}Suggested concepts #

A few concepts where produced, some of which were made into mock-ups.

## Visual mapping of articles to axes ##

![Visual mapping \label{visMap1}](../img/scaled/large/5_2_search_map.png)

![Visual mapping 2 \label{visMap2}](../img/scaled/large/5_3_search_map_drag_facets.png)

Some initial ideas explored putting search result items in a map like context, in order to visualize the grouping of items along facets in a continuous way. This could be particularly effective for facets that are of number type (See fig \ref{visMap1}). Such feature can allow the user to attain an overview of the distribution of properties over a dimension and also allow faster navigation by supplying an additional empowered level of freedom.

Additionally, through interaction, drag and drop or other, the user could be allowed to control both what facets are bound to each dimension; as well as what value should be brought into view if the interval would not fit on screen. The later could either be controlled by dragging the values along the axis or by dragging from a list of values to the axis (See fig \ref{visMap2}). The motivation for such feature could be that it would make it easy for the user to switch what dimensions are to be traversed when selecting out of the result set, and as such, allow the user to navigate a high number of dimensions more quickly.

## Incorporating multiple axes navigation into result list ##

![Horizontal drag scroll on facets \label{horDrag}](../img/scaled/large/3_2_2_drag_scroll.png)

A variation of axis mapping of facets is to have columns for each result group or facet value in an ordinary result list. Such solution could arguably be more friendly to an adaptive layout while somewhat loosing the aspect of communicating the distribution of items along a dimension (See figure \ref{horDrag}). It would still have the benefit of supplying an additional degree of freedom that can be used for navigation.

In order to make scrolling to adjacent groups drag scroll could be used. This can expand the interactive region as the user does not need to access periphery scroll controls and doesn't need to know about manipulator key such as shift-scroll. However, drag scroll has so far been avoided for most desktop inclined application; probability for acceptance might be limited.

## \label{PrefColl}Preference collection ##

Also referred to as filter tray. In order to separate various tasks and as such make the selection routine for large heterogeneous data sets less daunting, selection of properties and selection of their values can be made separate. As suggested by figure \ref{dragVal} properties would reside in a separate element from the so called filter tray. The filter tray would house those properties selected by the user to perform filtering on the result set. Each filter represent a property and supply the necessary controls for selecting a scope of values for that property to select on. Such preference collection can have the potential benefit of both reducing the number of elements shown and to bring the properties useful to the user closer at hand.

If expanding on the concept, there could be ways for the user to build customized collections of preferences. Perhaps as the user return to a category previously visited, the properties and values that was used last time around could be already selected and appearing in the filter tray.

## \label{DragnDrop}Creating custom filters by dragging from data elements ##

![Drag a value to create a filter \label{dragVal}](../img/scaled/large/7_8_1_multiple_facets_additional_horizontals.png)

As a proposed solution towards easing the process of narrowing down the result set; the user should be able to easily find and select properties and values. Properties may not always be findable in one place, they may live in various spaces of the GUI. The information displayed in the GUI comprise articles, properties or property values. All those elements could potentially be used for rephrasing a query. Properties and values could be made into filters and articles could be seen as a proxy for other keywords, properties or values; either from looking at its own properties, or by looking at its percolated values (See \nameref{Percolation}).

This gives the idea of allowing the creation of filters from such elements in order to ease exploratory search. For example, values present in the columns of the result list could be made draggable and turned into a facet filter when dropped in the list of filters (See figure \ref{dragVal}).

## \label{DynFilt}Dynamic filter lists ##

Filters, as mentioned above, can be managed in various ways to further lift the capabilities of the user. Rearranging such lists could be made meaningful. One proposition is to make the rearrangement of filter lists to influence the sorting of results. The top most filter, if it represent an interval, should decide the primary sorting feature. Consecutive filters will decide the sorting of items within each group of items that share the same value of the facet represented by the previous filter. For example, if there are two filters, 'price' and 'length', results will be sorted by price first and length second.

A second potential of *dynamic filter lists*, could be that of adapting the selectable range of values based on what ranges are currently available in the result set. If the price range is changed in the price filter, some part of the length range showing in the length filter may no longer be valid. As such, its selectable range should be adjusted. However, this may prove problematic as it can sometimes be desirable to show ranges that are no longer applicable. A solution to that however, could be to just 'grey-out' values that correspond to empty result sets.

## Expand with keywords ##

![Expand search using related keywords \label{relWord1}](../img/scaled/large/4_1_expand_search.png)

![Expand search using related keywords \label{relWord2}](../img/scaled/large/4_2_expand_search.png)

Another venture that could be explored is that of adding keywords to a query, as a lightweight way of exploratory search. There should be a fair amount of semantic reasoning behind the suggested words however. Direct synonyms should be searched implicitly and not be part of the suggestions. So suggested words should be related but not equivalent. Additionally, suggestions could be used in order to select among homonyms (see fig \ref{relWord2}).

In order to meet the need for semantic relatedness data, above suggested technology for using Wikipedia to that end could be used. While many other applications of the Wikipedia technology likely are possible, it is left out of the scope of this thesis.

## \label{Percolation}Percolation on article creation ##

![Percolation concept \label{Perc}](../img/scaled/large/percolation.png)

Percolation, an ElasticSearch specific feature, could potentially be used in a scenario where a user is about to create an article for a product, to inform the user how to expose an article for the most relevant search expressions. Percolation works by indexing selected queries so that documents can be tested onto them, seeing if using such query would turn that document into a valid result. This notion could be used to incentivise the user into tagging and otherwise writing the article for maximum precision. Although care should be placed in avoiding incentivising tag abuse instead. By only showing a few queries most relevant to the article this should be avoided. (See fig \ref{Perc})

Further, percolation could be used for analyzing the impact of entire product feeds coming from retailers. If used as such, it could help incentivise retailers into delivering cleaner feeds and also conveying value brought by the search engine. Also, manual tagging and categorization of multiple products at once could be aided. [@Percolator-examples]

Other uses could be thought of as well, but require further feasibility studies. Percolation could potentially bu used for aiding in maintaining and creating tags from queries for example.

## Sunflower navigation ##

![Search map/Sunflower \label{sunfl}](../img/scaled/large/5_5_search_map_expand_search.png)

Sunflower navigation expands on the expand with keywords concept and tries to map it onto dimensions; creating a graphical vector representation of each significant keyword in the query phrase and overlaying related, not yet selected, keywords, allowing the user to turn the suggestions into actual search keywords. (See figure \ref{sunfl})

For such concept to work, there needs to be an adjacency heuristic for each pair of tags. Arrows, or vectors can then be put into layout by force based means, to visualize concept closeness. This way, exploring related words can be made quicker as the user is encouraged to first select on subgroups rather than individual words.

<!--
- Some needs identified early, some later
- Clustering algorithms
- Infer categories
- Infer document categorization
- Using k-means or other algorithms on properties
- Mixing static and dynamic/derived categorization information
-->

<!--  LocalWords:  ElasticSearch Solr Lucene JSON Qalixa
 -->



