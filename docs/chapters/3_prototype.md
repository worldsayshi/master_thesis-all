

# \label{Prototype}Prototype #

## User interface

![User interface components \label{guiScreen}](../img/scaled/large/guiScreenshot-annotated.png)

The prototype selects from various previously mentioned concepts. Concepts that were focused on were \ref{PrefColl} and \ref{DynFilt}. \ref{DragnDrop} was tried to some extent but discarded as the overall design did not supply enough useful data elements for such concept to be useful. An additional set of features not outlined above, such as \ref{DynCol} and \ref{FuzzyFacets}, were implemented and are described below.

The client consist of a search form, a side pane populated with groups of attributes applicable for the search phrase currently entered, a list of results and a "filter list". The latter being initially hidden.

The attribute list orders attributes in small groups of some conceptual similarity determined by the clustering algorithm. If the user selects an attribute from the list, it will be added to the filter list. The internal representation of the filter list is traversed when changed to produce a new query once a property value has been selected. Filter components can be rearranged by drag and drop. (See fig. \ref{guiScreen})

## Client architecture

![Ideal representation of the application model \label{idealM}](../img/scaled/small/idealModel.png)

![UML representation of the application model \label{umlM}](../img/scaled/small/client-uml.png)

An ideal representation of the application model can be seen in fig \ref{idealM}. The search form is the initial interactive component for the user. The search form submit an input phrase to the search framework interface that in turn communicate with the search server. The interface notify both result list and facet list when a response is retrieved. The user can then interact with the facet list to add filters to the filter list. The filter list notifies the search framework when a meaningful change has happened and again the facet and result list is updated. Optionally, filter content is updated as well.

The actual implementation (see fig \ref{umlM}) consist of an ApplicationModel that house all other models except the search interface, SolrInterface. It also handle most of the event bindings and propagations. The application model contain the collection of Attributes, the collection of Filters, the collection of Results, the collection of Groupings and the Query model. The Attributes collection carry the most model logic, collecting all Attribute models. The Attribute model in turn each own a collection of Values that hold Value models. The Grouping and Filter collection on the other hand only hold references in the form of String ids. At any time when an attribute that form a filter needs to modify its model, it has to be found inside the Attributes collection. This was considered a necessary detail at the time of writing, due to the workings of Backbone, but might be done more elegantly using actual references instead of ids.

There are several view classes used to reflect the state of the models in the GUI. The primary ones are the FacetList, the FilterList and the ResultList. In addition there is FilterComponent, being instantiated as subviews of the FilterList and the SearchForm, representing the form element used for search phrase input.

In addition to the above, there are toolbars. As models and as a view. These are added to allow removing and *locking* filters. The locking of filters denoting locking selectable values from being changed as the result set is changing. [@GuiSource]

<!--
- Ideal model (fig \ref{idealM})
	- search form, facet list, filter list, articles, search framework
- Implementation (fig \ref{umlM})
	- Models
		- Attributes, Attribute
		- Values, Value
		- Groupings
		- Results
		- Filters
		- Application model
		- Query
		- Solr Interface
	- Views
		- Facet list
		- Filter list
		- Result list
		- Toolbars
    - Other Classes or modules
		- Filter processor
		- main module
 - Framework: Backbone

The client architecture try to build upon Backbone MW-idioms that can be found within the community of users[@BackboneUsers]. The application model consist of a collection of attributes that each hold a collection of values. 
-->

## Algorithmic needs ##

![Property adjacencies from articles \label{adj}](../img/scaled/large/grouping1.png)

![Adjacency matrix \label{mtr}](../img/scaled/large/adjacency.png)

![Clustering using centroids showing the three first dimensions/properties \label{centr} ](../img/scaled/small/clustering.png)

Looking at challenges that need to be met and the solutions proposed, some algorithmic needs could be discerned.
In order to make the data workable and coherent, documents and attributes need to be matched with near equivalents. Synonyms, homonyms and polysemic words need to be recognized somehow. Articles need to be fitted into a suitable category. Articles may be coming from divergent sources, referencing diverse category structures and strictly incompatible taxonomies. Sometimes articles may supply little or no pre existing category assignment and perhaps data to go by.

It should be desirable to automatically extend an existing category structure in some cases, for example when a category is considered to overflow, when there are too many items in a category for it being easily navigated. It could perhaps be desirable to show and hide various branches of a complex category tree depending for example on the size of the branches given a search phrase.

Given a large amount of properties given by search results, to be selected from for allowing further refinement, there needs to be ways of give structure in order to lower the complexity faced by the user. Applying clustering techniques could meet this problem to some extent.

### Grouping Algorithm ###

As a partial response for the need of adding some additional structure, a grouping algorithm was implemented. It is is added to the Solr execution as a plugin component to a custom search handler in the Solr configuration. The major part of the algorithm runs as a response to a query, processing the set of articles returned by Solr and producing the relevant output that is attached to the result object returned to the client.

The algorithm begin by generating a adjacency matrix of all the properties of articles of the result set, incrementing the adjacency for every time two properties occur together (Fig \ref{adj}). The adjacency matrix is then interpreted as an Eucledian space with as many dimensions as there are properties (Fig \ref{mtr}) and passed into an implementation of Lloyd's k-means clustering algorithm, from which a set of groups/clusters are produced (Fig \ref{centr}). Below is a simplified version of the clustering algorithm in pseudo code:

````

lloyd = function () {
centroids := randomCentroids
until (time_out or goodEnough):
	for each point:
		assignToCluster(point)
	centroids := (for each cluster:
		recompute centroid of cluster)
}

assignToCluster = function (point) {
closestCluster := find cluster closest to point
assign point to closestCluster
}

````

The algorithm is done when the time has run out or when the centroids move sufficiently little per iteration.

<!--
### Modules
 - FieldGroupingComponent
 - Kruskal
 - Lloyd
 - Test
 - Common
-->

## Data importer
A data importer was needed on the server side to correctly and efficiently transfer data from the original MySql database to Solr. While a standardized data import plugin exist for Solr, it was deemed a bad fit for the needs of this project. Import schemes made for the standard importer ran slow and took away the possiblity of multi valued properties. The custom importer showed a very significant speedup. While there may be means of reaching the same speedup without custom code it could not be found.

### Modules
The import handler consist of a worker and a front end class. The front end class, FlatTableImportHandler, is called from Solr when there is an url request to the corresponding request handler specified in the Solr configuration. With the url a command is passed. If the command is "full-import" the worker is started in a thread of its own. Any other command, or the lack of a command will return a status report for the worker.

The worker, ImportWorker, execute a MySql query specified in the Solr configuration and read the response line by line. As long as a row has the same unique id, properties specified by the row will aggregate into a document. The document will be submitted to Solr once the id has changed. This is done so that the document in the Solr database will not risk being overwritten.

## Feature roundup

### Filter selection and deletion
The user specify property preferences of the sought results by chosing properties from a list. Chosen properties will turn into filters that also display applicable values for the result set, allowing them to be chosen from. Choosing one or more values will cause the document set to be filtered to match the property-value pairs chosen. In non fuzzy mode, it should match at least one property-value combination per filter to not be excluded.

### Switchable filter value updates (Padlocks)
In order to give a dynamic view of what properties remain meaningful as preferences are specified, the padlock button allows switching whether the set of values for a particular property should be dynamically updated as the result set changes. The set of values that remain if the filter is *unlocked* corresponds to those property values that can be found in the result set.

### Sliders for enumerable facets (deselecting intermediate values?)
Some properties are recognized as *enumerable*, or of number type. It should in most cases be interesting for the user to select a span rather than individual values for such cases, avoiding the extra work of selecting a large number of similar values. Therefore, a slider is used for controlling such value selection.

### Switchable sorting and sorting customization by drag and drop
Without adding additional elements to the scene, a fine grained control for the sorting order of results is made possible by looking at the ordering of filters. For an attentive user, it can be seen that the sorting order is made by each column in order. The rationale for this is that the prioritization of the filters, or *preferences* of the user, can be signified by the visual order. The order of the columns in the result list is determined by the order of filters as well. The filter order can be rearranged by drag and drop.

### \label{DynCol}Dynamic column creation
What properties are to be displayed in the result list is determined in part by a predefined list of default properties and in part by the properties corresponding to the filters in the filter list. This allows a dynamic display of articles where focus is put on those properties that are likely of most interest to the user.

<!-- new concept: expand with "similar properties"? -->

### \label{FuzzyFacets}Fuzzy facets
The user has an option of enabling *facet fuzziness*. This causes the filters to become promoting/demoting of matching/non-matching articles rather than excluding filtered articles entirely. This feature is realized with Solr using query boosting. For example, the following generated query will only retrieve articles that match the preferences exactly:

````
car AND (color:blue AND price:[100 TO 10000] AND model:Toyota)
````

However, adding wildcard queries with a lower prioritization/boost will allow previously unmatched articles to appear towards the end of the result list:

````
(car AND (color:blue^1000 OR color:[* TO *]^0.1) 
     AND (price:[* TO 10000]^0.9 OR price:[* TO *]^0.1) 
     AND (model:Toyota^1000 OR model:[* TO *]^0.1))
````

There is a reservation on this solution; articles that does not define the property will still not be included. Depending on the reliability of the data this may or may not be desirable. An addition that should include even such articles with an even lower prioritization would be as below. This possiblity is not demonstrated in the prototype however.

````
(car AND (color:blue^1000 OR color:[* TO *]^10) 
     AND (price:[* TO 10000]^0.9 OR price:[* TO *]^10) 
     AND (model:Toyota^1000 OR model:[* TO *]^10)) 
 OR car^1
````

<!--
#### Fuzzy faceted filtering [DEPRECATED, it's now a feature!]
Normally, faceted filtering exclude, in the absolute sense, articles that does not fit given preferences. When looking at a data base that contain a lot of missing and malformed properties, this should not be desirable. Rather, it would be desirable to have documents ranked according to how well they fit given filter settings. 

Fuzzy search is a common theme in Information Retrieval. However, most often it denotes searching on similar strings rather than meeting requirements in a *more or less* sense. 

To avoid some filtering out some false negatives where the user is filtering on a property that is not defined for the document, there may be a way to avoid filtering on those occasions. Perhaps a 'force' option could be added to the filter or to a generic options panel, to allow the user to toggle this functionality for individual or for all properties. Such article should perhaps be considered less relevant. Gentle faceting may be incompatible with some search frameworks, or require changes in core modules of such a framework.
-->

<!--
## Challenges faced
- Data import performance
- GUI architecture design
- Code maintainability
- Quality of user experience vs features and experimentation
-->
