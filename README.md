### **Note:**
Submodules are not linked to correctly in the source view here on BitBucket. It is believed to follow from [this](https://bitbucket.org/site/master/issue/4162/in-the-browser-repository-https-clone-urls) issue.
The submodules can be accessed here in the meantime: [client](https://bitbucket.org/worldsayshi/master_thesis-gui),[grouping algorithm](https://bitbucket.org/worldsayshi/fieldgroupingcomponent), [import handler](https://bitbucket.org/worldsayshi/flattableimporthandler)

# Small summary:
## Flat table import handler (server/customComponents/dataImport)
For importing data from MySql database to Solr
## Field grouping component (server/customComponents/fieldGroupingComponent)
For clustering of attributes when submitting a search query. Has two grouping algorithms that are switchable by a constant in the code (FieldGroupingComponent.groupingScheme). 
## Master thesis gui/web client (client)
Web front end that talks with solr. Using backbone, underscore, jquery and some jquery-ui.

All these components are designed to work alone reasonably well. They should depend on each other only run time wise. Data importer depends on Solr. Grouping component depends on the data importer. Client depends on grouping component (although search should work without grouping, possibly you need to disable grouping in Solr config).

# Known issues
As far as known, all of the known buggy behaviors can be escaped by reloading the page.

- Selecting some attribute values containing whitespaces or unconventional symbols may cause malformed query to solr, failure to update or wrong results. There is now a workaround in place where whitespaces are replaced with underscores behind the curtains.
- The multi valued-ness of properties currently needs to be set in two places in the solr configuration. In the configuration of the import handler and in the schema. It would be better if it could be set only in the schema.
- For some queries Solr responds with a null object. However, at other times 'no documents found' results in a result object with no documents. The later behavior seems the correct one. This behavior has not been reproducible in later project snapshots.
- The current schema produce a extremely large database. Consider tweaking the schema and removing long descriptions from index. It is currently only possible to save some portion of the qalixa database to disk. The size of the index on disk may fluctuate as Solr runs its indexing algorithms upon running *commit*.

## Set up:
### Assumptions
- apache 2 running on machine
- mysql 5.6>= running on machine with database adherent to config in server/solrconfig

### How to set up server
- fetch submodules
- run getSolr in server folder
- run runServer to start server
- fill up the database by making data import query to solr:
localhost:8983/solr/flattable-dataimport?command=full-import
- Check its progress by visiting:
localhost:8983/solr/flattable-dataimport
- If the data is begining to take up too much space, you can abort and commit with
localhost:8983/solr/flattable-dataimport?command=stop
 
### How to set up client
- Configure apache to point at the client folder
- Go to localhost in the browser
- Use the gui!

### Build data importer and grouping module
- You *should* not need to do this to run. There are pre built jar-binaries in server/solrconfig/lib.
- If you want to update the jars. Run the ant build scripts in each eclipse project, default target.

### Gotchas
- *property*, *attribute* and *facet* are names used interchangeably throughout the source code and thesis.
  They have essentially the same meaning but may be used to emphasis the use context. *Facet* is usually meant as a property that can be used for filtering a set of documents but may sometimes also connote *a property with an associated set of values*. *Filter* and *field* are other overlapping concepts. *Filter* is a facet that is currently being used for filtering. *Field* is a synonym for *property/attribute* in most cases but may be used with another meaning depending on context.
